<!ENTITY layer-base SYSTEM "layer-base.xml.inc">
<!ENTITY layer-landmark SYSTEM "layer-landmark.xml.inc">
<!ENTITY layer-hillshade SYSTEM "layer-hillshade.xml.inc">
<!ENTITY layer-water SYSTEM "layer-water.xml.inc">
<!ENTITY layer-transportation SYSTEM "layer-transportation.xml.inc">
<!ENTITY layer-buildings SYSTEM "layer-buildings.xml.inc">
<!ENTITY layer-ski SYSTEM "layer-ski.xml.inc">
<!ENTITY layer-text SYSTEM "layer-text.xml.inc">
<!ENTITY layer-ski-labels SYSTEM "layer-ski-labels.xml.inc">

